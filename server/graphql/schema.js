const { gql } = require('apollo-server-express')

module.exports = gql`
	type Query {
		getCategories: [Category]
		getCuisines: [Cuisine]
		getRestaurants: [Restaurant]
		getRestaurant (id: String!): Restaurant
		getBooking: [Booking]
		getBookings: [Booking]
	}

	type Mutation {
		addCategory (name: String!): Category
		addCuisine (name: String!): Cuisine
		addUser (firstName: String!, lastName: String!, email: String!, contact: String!, password: String!): Boolean
		addRestaurant (name: String!, description: String!, place: String!, unitPrice: Float!, cuisine: String!, map: String! base64EncodedImage: String!): Boolean
		login (email: String!, password: String!) : User
		updateRestaurant (id: String!, name: String!, description: String!, place: String!, unitPrice: Float!, cuisine: String!, map: String!): Boolean
		deleteRestaurant (id: String!): Boolean
		addBooking (
			userId: String!, 
			userFirstName: String!,
			userLastName: String!, 
			restaurantId: String!, 
			restaurantName: String!, 
			restaurantPlace: String!, 
			restaurantUnitPrice: Float!, 
			date: String!, 
			category: String!, 
			paymentMode: String!
		) : Boolean
	}

	type Category {
		id: ID!
		name: String!
	}

	type Cuisine {
		id: ID!
		name: String!
	}

	type Restaurant {
		id: ID!
		name: String!
		description: String!
		place: String!
		unitPrice: Float!
		cuisine: String!
		map: String!
		imageLocation: String!
	}

	type History {
		id: ID!
		userId: String!
		restaurantId: String!
		amountPaid: Float!
		createdAt: String!
		dateAvailed: String!
	}

	type User {
		id: ID!
		firstName: String!
		lastName: String!
		email: String!
		contact: String!
		role: String!
		stripeCustomerId: String!
		token: String
	}

	type Booking {
		id: ID!
		userId: String!
		userFirstName: String!
		userLastName: String!
		restaurantId: String!
		restaurantName: String!
		restaurantPlace: String!
		restaurantUnitPrice: Float!
		date: String!
		category: String!
		paymentMode: String!
	}
`