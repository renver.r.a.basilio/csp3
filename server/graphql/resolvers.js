const bcrypt = require('bcrypt')
const auth = require('../jwt-auth')
const fs = require('fs')
const uuid = require('uuid/v1')
const stripe = require('stripe')('sk_test_CSHkpdtPB4QPSwxr62CHqTUx00dgAuI7mB')

const Category = require('../models/category')
const Cuisine = require('../models/cuisine')
const User = require('../models/user')
const Restaurant = require('../models/restaurant')
const Booking = require('../models/booking')

module.exports = {
	Query: {
		getCategories: (parent, args) => {
			return Category.find({})
		},
		getCuisines: (parent, args) => {
			return Cuisine.find({})
		},
		getRestaurants: (parent, args) => {
			return Restaurant.find({ isArchived: false })
		},
		getRestaurant: (parent, args) => {
			return Restaurant.findById(args.id)
		},
		getBooking: (parent, args, context) => {
			const userId = context.currentUser._id
			const role = context.currentUser.role

			return Booking.find({ userId: userId})
		},
		getBookings: (parent, args) => {
			return Booking.find({})
		},
	},
	Mutation: {
		addCategory: (parent, args) => {
			let category = new Category({
				name: args.name
			})

			return category.save()
		},
		addCuisine: (parent, args) => {
			let cuisine = new Cuisine({
				name: args.name
			})

			return cuisine.save()
		},
		addUser: (parent, args) => {
			return stripe.customers.create({
				email: args.email,
				source: 'tok_mastercard',
			}).then((customer, err) => customer).then((customer) => {
				let user = new User({
					firstName: args.firstName,
					lastName: args.lastName,
					email: args.email,
					contact: args.contact,
					password: bcrypt.hashSync(args.password, 10),
					role: 'customer',
					stripeCustomerId: customer.id
				})
				return user.save().then((user) => {
					return (user) ? true : false
				})
			}).catch(() => {
				return false
			})

		},
		login: (parent, args) => {
			let query = User.findOne({ email: args.email })

			return query.then((user) => user).then((user) => {
				if(user === null) { return null }

				let isPasswordMatched = bcrypt.compareSync(args.password, user.password)

				if (isPasswordMatched) {
					user.token = auth.createToken(user.toObject())
					return user
				} else {
					return null
				}
			})
		},
		addRestaurant: (parent, args) => {
			console.log(args.base64EncodedImage)

			let base64Image = args.base64EncodedImage.split(';base64,').pop()
			let imageLocation = 'images/' + uuid() + '.png'

			fs.writeFile(imageLocation, base64Image, { encoding: 'base64' }, (err) => {})

			let restaurant = new Restaurant({
				name: args.name,
				description: args.description,
				place: args.place,
				unitPrice: args.unitPrice,
				cuisine: args.cuisine,
				map: args.map,
				imageLocation
			})

			return restaurant.save().then((item) => {
				console.log(item)
				return (item) ? true : false
			})
		},
		updateRestaurant: (parent, args) => {
			let condition = { _id: args.id }
			let updates = {
				name: args.name,
				description: args.description,
				place: args.place,
				unitPrice: args.unitPrice,
				cuisine: args.cuisine,
				map: args.map
			}

			return Restaurant.findOneAndUpdate(condition, updates)
			.then((restaurant, err) => {
				return (err) ? false : true
			})
		},
		deleteRestaurant: (parent, args) => {
			let condition = { _id: args.id }
			let updates = { isArchived: true }

			return Restaurant.findOneAndUpdate(condition, updates)
			.then((item, err) => {
				return (err) ? false : true
			})
		},
		addBooking: (parent, args, context) => {
			const stripeCustomerId = context.currentUser.stripeCustomerId

			if (args.paymentMode == 'Cash') {
				return checkoutCOD(args)
			} else if (args.paymentMode == 'Stripe') {
				return checkoutStripe(args, stripeCustomerId)
			} else {
				return false
			}

		}
	}
}

const checkoutCOD = (args) => {

	let booking = new Booking({
		userId: args.userId,
		userFirstName: args.userFirstName,
		userLastName: args.userLastName,
		restaurantId: args.restaurantId,
		restaurantName: args.restaurantName,
		restaurantPlace: args.restaurantPlace,
		restaurantUnitPrice: args.restaurantUnitPrice,
		date: args.date,
		category: args.category,
		paymentMode: args.paymentMode
	})
	booking.save()

	return true
}

const checkoutStripe = (args, stripeCustomerId) => {
	return stripe.charges.create({
		amount: args.restaurantUnitPrice * 100,
		description: null,
		currency: 'php',
		customer: stripeCustomerId
	}).then((charge, err) => charge).then((charge) => {
		let booking = new Booking({
			userId: args.userId,
			userFirstName: args.userFirstName,
			userLastName: args.userLastName,
			restaurantId: args.restaurantId,
			restaurantName: args.restaurantName,
			restaurantPlace: args.restaurantPlace,
			restaurantUnitPrice: args.restaurantUnitPrice,
			date: args.date,
			category: args.category,
			paymentMode: args.paymentMode
		})
		booking.save()

		return true
	}).catch((err) => {
		return false
	})
}