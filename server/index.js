const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const { ApolloServer } = require('apollo-server-express')
const gqlSchema = require('./graphql/schema')
const gqlResolvers = require('./graphql/resolvers')
const auth = require('./jwt-auth')

const app = express()

app.use(bodyParser.json({ limit: '5mb' }))
app.use('/images', express.static('images'))

const apolloServer = new ApolloServer({
	typeDefs: gqlSchema,
	resolvers: gqlResolvers,
	context: ({ req }) => {
		const token = req.headers.authorization
		const currentUser = auth.verify(token)

		return { currentUser }
	}
})

apolloServer.applyMiddleware({ app, path: '/graphql'})

mongoose.connect('mongodb+srv://csp3:csp3123456789@csp3-trmnw.mongodb.net/CSP3?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => {
	console.log('Now connected to local MongoDB server.')
})

app.listen({ port: 5000 }, () => {
	console.log('Express server is now running.')
	dbCheck()
})

const dbCheck = () => {
	const User = require('./models/user')
	const bcrypt = require('bcrypt')

	User.find({email: 'admin@gmail.com'}, (err, users) => {
		if (users.length === 0) {
			let user = new User({
				firstName: 'admin@gmail.com',
				lastName: 'admin@gmail.com',
				contact: 123456789,
				email: 'admin@gmail.com',
				password: bcrypt.hashSync('admin@gmail.com', 10),
				role: 'admin',
				stripeCustomerId: 'default_id'
			})

			user.save()
			console.log('Admin account not found. Automatically created one.')
		}
	})
}