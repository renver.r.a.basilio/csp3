const mongoose = require('mongoose')
const Schema = mongoose.Schema

const historySchema = new Schema({
	userId: {
		type: String,
		required: true
	},
	restaurantId: {
		type: String,
		required: true
	},
	amountPaid: {
		type: Number,
		required: true
	},
	createdAt: {
		type: Date,
		default: new Date()
	},
	dateAvailed: {
		type: Date,
		required: true
	}
})

module.exports = mongoose.model('history', historySchema)