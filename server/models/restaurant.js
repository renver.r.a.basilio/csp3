const mongoose = require('mongoose')
const Schema = mongoose.Schema

const restaurantSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	place: {
		type: String,
		required: true
	},
	unitPrice: {
		type: Number,
		required: true
	},
	cuisine: {
		type: String,
		required: true
	},
	imageLocation: {
		type: String,
		required: true
	},
	map: {
		type: String,
		required: true
	},
	isArchived: {
		type: Boolean,
		default: false
	}
}, {
	timestamps: true
})

module.exports = mongoose.model('restaurant', restaurantSchema)