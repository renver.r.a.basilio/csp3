const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bookingSchema = new Schema({
    userId: {
        type: String,
        required: true
    },
    userFirstName: {
        type: String,
        required: true
    },
    userLastName: {
        type: String,
        required: true
    },
    restaurantId: {
        type: String,
        required: true
    },
    restaurantName: {
        type: String,
        required: true
    },
    restaurantPlace: {
        type: String,
        required: true
    },
    restaurantUnitPrice: {
        type: Number,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    paymentMode: {
        type: String,
        required: true
    }
}, {
	timestamps: true
})

module.exports = mongoose.model('booking', bookingSchema)