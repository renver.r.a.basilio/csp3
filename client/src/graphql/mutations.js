const Mutation = {
	addUser: `
		mutation (
			$firstName: String!,
			$lastName: String!,
			$email: String!,
			$contact: String!,
			$password: String!
		) {
			addUser (
				firstName: $firstName,
				lastName: $lastName,
				email: $email,
				contact: $contact,
				password: $password
			)
		}
	`,
	login: `
		mutation (
			$email: String!,
			$password: String!
		) {
			login (
				email: $email,
				password: $password
			) {
				id
				firstName
				lastName
				role
				token
			}
		}
	`,
	addRestaurant: `
		mutation (
			$name: String!,
			$description: String!,
			$place: String!,
			$unitPrice: Float!,
			$cuisine: String!,
			$map: String!,
			$base64EncodedImage: String!
		) {
			addRestaurant (
				name: $name,
				description: $description,
				place: $place,
				unitPrice: $unitPrice,
				cuisine: $cuisine,
				map: $map,
				base64EncodedImage: $base64EncodedImage
			)
		}
	`,
	updateRestaurant: `
		mutation (
			$id: String!,
			$name: String!,
			$description: String!,
			$place: String!,
			$unitPrice: Float!,
			$cuisine: String!,
			$map: String!
		) {
			updateRestaurant (
				id: $id,
				name: $name,
				description: $description,
				place: $place,
				unitPrice: $unitPrice,
				cuisine: $cuisine,
				map: $map
			)
		}
	`,
	deleteRestaurant: `
		mutation ( 
			$id: String!
		) {
			deleteRestaurant (
				id: $id
			)
		}
	`,
	addBooking: `
		mutation (
			$userId: String!,
            $userFirstName: String!,
            $userLastName: String!,
            $restaurantId: String!,
            $restaurantName: String!,
            $restaurantPlace: String!,
            $restaurantUnitPrice: Float!,
			$date: String!,
			$category: String!
			$paymentMode: String!
		) {
			addBooking (
				userId: $userId,
            	userFirstName: $userFirstName,
            	userLastName: $userLastName,
            	restaurantId: $restaurantId,
            	restaurantName: $restaurantName,
            	restaurantPlace: $restaurantPlace,
            	restaurantUnitPrice: $restaurantUnitPrice,
				date: $date,
				category: $category
				paymentMode: $paymentMode
			)
		}
	`
}

export default Mutation