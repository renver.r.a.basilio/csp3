const Query = {
    getCategories: `
    	{
    		getCategories {
    			id
    			name
    		}
    	}
    `,
    getCuisines: `
        {
            getCuisines {
                id
                name
            }
        }
    `,
    getRestaurants: `
        {
            getRestaurants {
                id
                name
                description
                place
                cuisine
                unitPrice
                imageLocation
            }
        }
    `,
    getRestaurant: `
        query (
            $id: String!
        ) {
            getRestaurant (
                id: $id
            ) {
                name
                description
                place
                cuisine
                unitPrice
                map
                imageLocation
            }
        }
    `,
    getBooking: `
        {
            getBooking {
                id
                date
                category
                restaurantName
                restaurantPlace
                restaurantUnitPrice
                paymentMode
            }
        }
    `,
    getBookings: `
        {
            getBookings {
                id
                userFirstName
                userLastName
                restaurantName
                restaurantUnitPrice
                date
                category
                paymentMode
            }
        }
    `,
}

export default Query
