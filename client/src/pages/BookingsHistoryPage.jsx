import React, { useState, useContext} from 'react'

import Container from 'react-bootstrap/Container'
import Table from 'react-bootstrap/Table'

import Query from '../graphql/queries'
import { GQLClient } from '../helpers'
import { useEffect } from 'react'

const BookingsHistoryPage = (props) => {
    const [bookings, setBookings] = useState([])

    useEffect(() => {
        GQLClient({}).request(Query.getBookings).then((data) => {
            let bookingRows = data.getBookings.map((booking) => {
                return (
                    <tr key={booking.id}>
                        <td>{ booking.userFirstName }</td>
                        <td>{ booking.id }</td>
                        <td>{ booking.category }</td>
                        <td>{ booking.restaurantName }</td>
                        <td>{ booking.restaurantUnitPrice }</td>
                        <td>{ booking.paymentMode }</td>
                        <td>{ new Date(parseInt(booking.date)).toLocaleDateString() } </td>
                    </tr>
                )
            })

            setBookings(bookingRows)
        })
    }, [])
    return (
        <Container fluid>
            <h3>Bookings</h3>
            <Table bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Booking ID</th>
                        <th>Meal Time</th>
                        <th>Restaurant Name</th>
                        <th>Price</th>
                        <th>Payment Mode</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    { bookings }
                </tbody>
            </Table>
        </Container>
    )
}

export default BookingsHistoryPage