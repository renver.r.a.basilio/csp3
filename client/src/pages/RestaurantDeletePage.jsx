import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'

import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'
import { GQLClient} from '../helpers'

import '../index.css'

const RestaurantDeletePage = (props) => {
    const [isRedirected, setIsRedirected] = useState(false)

    if (isRedirected) {
        return <Redirect to='/'/>
    }

    return (
        <Container className="form-max-width">
            <h3>Delete Restaurant</h3>
            <Card>
                <Card.Header>Restaurant Information</Card.Header>
                <Card.Body>
                    <RestaurantDeleteForm restaurantId={ props.match.params.id }setIsRedirected={ setIsRedirected }/>
                </Card.Body>
            </Card>
        </Container>
    )
}

const RestaurantDeleteForm = ({ restaurantId, setIsRedirected }) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [place, setPlace] = useState('')
    const [unitPrice, setUnitPrice] = useState(0)
    const [cuisine, setCuisine] = useState(undefined)
    const [map, setMap] = useState('')

    useEffect(() => {
        GQLClient({}).request(Query.getRestaurant, { id: restaurantId }).then((data) => {
            setName(data.getRestaurant.name)
            setDescription(data.getRestaurant.description)
            setPlace(data.getRestaurant.place)
            setUnitPrice(data.getRestaurant.unitPrice)
            setCuisine(data.getRestaurant.cuisine)
            setMap(data.getRestaurant.map)
        })
    }, [])
    
    const deleteRestaurant = (e) => {
        e.preventDefault()

        GQLClient({}).request(Mutation.deleteRestaurant, { id: restaurantId }).then((data) => {
            if (data.deleteRestaurant) {
                Swal.fire({
                    title: 'Restaurant Deleted',
                    text: 'You will be redirected to the menu after closing this message',
                    icon: 'success'
                }).then(() => {
                    setIsRedirected(true)
                })    
            } else {
                Swal.fire({
                    title: 'Restaurant Delete Failed',
                    text: 'The server encountered an error.',
                    icon: 'error'
                })
            }
        })    
    }

    return (
        <Form onSubmit={ (e) => deleteRestaurant(e) }>
            <Form.Group>
                <Form.Label>Item Name</Form.Label>
                <Form.Control type="text" value={ name } disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" value={ description } disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Place</Form.Label>
                <Form.Control type="text" value={ place } disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Price</Form.Label>
                <Form.Control type="number" value={ unitPrice } disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Cuisine</Form.Label>
                <Form.Control type="text" value={ cuisine } disabled/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Map Location</Form.Label>
                <Form.Control type="text" value={ map } disabled/>
            </Form.Group>
            <Button type="submit" variant="danger">Delete</Button>&nbsp;
            <Button type="submit" variant="warning" href='/'>Cancel</Button>
    </Form>
    )
}

export default RestaurantDeletePage
