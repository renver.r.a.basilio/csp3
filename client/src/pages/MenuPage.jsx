import React, { useState, useEffect, useContext } from 'react'
import UserContext from '../contexts/UserContext'

import ButtonGroup from 'react-bootstrap/ButtonGroup'
import Container from '@material-ui/core/Container'
import Row from 'react-bootstrap/Row'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import Query from '../graphql/queries'
import { GQLClient, NodeServerURL } from '../helpers'

const MenuPage = (props) => {

  	const [restaurants, setRestaurants] = useState([])
  	const { user } = useContext(UserContext)
  	const btnAddRestaurant = <Button variant="contained" color="primary" href="/restaurant-create">Add Restaurant</Button>

  	useEffect(() => {
  		GQLClient({}).request(Query.getRestaurants, null).then(({ getRestaurants }) => {
  			setRestaurants(getRestaurants.map((restaurant) => <MenuRestaurant key={ restaurant.id } restaurant={restaurant}/>))
  		})
  	}, [])

  	return (
  		<Container>
            <h3 className="text-center">Restaurants</h3>
            <h4>{ (user.role === 'admin') ? btnAddRestaurant : null }</h4>
            <Row>
                { restaurants }
            </Row>
        </Container>
  	)

}

const MenuRestaurant = ({ restaurant }) => {
	const { user } = useContext(UserContext)
  	let btnRestaurantActions = ''

  	if (user.role === 'admin') {
  		btnRestaurantActions = (
  			<div className="d-flex flex-column mt-3">
	  			<ButtonGroup>
	  			    <Button variant="contained" color="primary" href={ "/restaurant-update/" + restaurant.id }>Edit</Button>&nbsp;
	  			    <Button variant="contained" color="primary" href={ "/restaurant-delete/" + restaurant.id }>Delete</Button>
	  			</ButtonGroup>
  			</div>
  		)
  	} else if (user.role === 'customer') {
        btnRestaurantActions = (
            <div className="d-flex flex-column mt-3">
	  			<Button variant="contained" color="primary" href={ "/booking/" + restaurant.id }>Book</Button>
  			</div>
        )
    }

  	return (
  		<Container className="col-4">
	    	<Card>
	      		<CardActionArea>
		        	<CardMedia
		          		component="img"
		          		height="140"
		          		src={ NodeServerURL + restaurant.imageLocation }
		          		title="Vikings"
		        	/>
		        	<CardContent>
		         		<Typography gutterBottom variant="h6" className="text-center">
		            		{ restaurant.name } @ { restaurant.place }
		         		</Typography>
		          		<Typography variant="body2" color="textSecondary" component="p">
		            		Location: { restaurant.place }
		          		</Typography>
		          		<Typography variant="body2" color="textSecondary" component="p">
		            		Price: &#8369;{ restaurant.unitPrice }
		          		</Typography>
		          		<br/>
		          		<Typography variant="body2" color="textSecondary" component="p">
		            		Reservations: 50
		          		</Typography>
		          		{ btnRestaurantActions }
		        	</CardContent>
	      		</CardActionArea>
	    	</Card>
	    </Container>
  	)
}

export default MenuPage