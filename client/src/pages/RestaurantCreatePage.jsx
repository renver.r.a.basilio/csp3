import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'

import Swal from 'sweetalert2'
import Container from 'react-bootstrap/Container'
import Card from 'react-bootstrap/Card'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'
import { GQLClient, toBase64 } from '../helpers'

import '../index.css'

const RestaurantCreatePage = () => {
    const [isRedirected, setIsRedirected] = useState(false)

    if (isRedirected) {
        return <Redirect to='/'/>
    }

    return (
        <Container className="form-max-width">
            <h3>New Item</h3>
            <Card>
                <Card.Header>Restaurant Information</Card.Header>
                <Card.Body>
                    <RestaurantCreateForm setIsRedirected={ setIsRedirected }/>
                </Card.Body>
            </Card>
        </Container>
    )
}

const RestaurantCreateForm = ({ setIsRedirected }) => {
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [place, setPlace] = useState('')
    const [unitPrice, setUnitPrice] = useState(0)
    const [cuisine, setCuisine] = useState(undefined)
    const [cuisines, setCuisines] = useState([])
    const [map, setMap] = useState('')
    const fileRef = React.createRef()

    useEffect(() => {
        GQLClient({}).request(Query.getCuisines, null).then((data) => {
            let options = data.getCuisines.map((cuisine) => {
                return <option key={ cuisine.id } value={ cuisine.name }>{ cuisine.name }</option> 
            })
            setCuisines(options)
        })
    }, [])
    
    const createRestaurant = (e) => {
        e.preventDefault()

        toBase64(fileRef.current.files[0]).then((encodedFile) => {
            let variables = {
                name,
                description,
                place,
                unitPrice: parseFloat(unitPrice),
                cuisine,
                map,
                base64EncodedImage: encodedFile
            }

            GQLClient({}).request(Mutation.addRestaurant, variables).then((data) => {
                if (data.addRestaurant) {
                    Swal.fire({
                        title: 'Item Added',
                        text: 'You will be redirected to the menu after closing this message',
                        icon: 'success'
                    }).then(() => {
                        setIsRedirected(true)
                    })    
                } else {
                    Swal.fire({
                        title: 'Item Add Failed',
                        text: 'The server encountered an error.',
                        icon: 'error'
                    })
                }
            })  
        })
    }

    return (
        <Form onSubmit={ (e) => createRestaurant(e) }>
            <Form.Group>
                <Form.Label>Item Name</Form.Label>
                <Form.Control type="text" value={ name } onChange={ (e) => setName(e.target.value) }/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Description</Form.Label>
                <Form.Control type="text" value={ description } onChange={ (e) => setDescription(e.target.value) }/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Place</Form.Label>
                <Form.Control type="text" value={ place } onChange={ (e) => setPlace(e.target.value) }/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Price</Form.Label>
                <Form.Control type="number" value={ unitPrice } onChange={ (e) => setUnitPrice(e.target.value) }/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Cuisine</Form.Label>
                <Form.Control as="select" value={ cuisine } onChange={ (e) => setCuisine(e.target.value) }>
                    <option value selected disabled>Select Cuisine</option>
                    { cuisines }
                </Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Map Location</Form.Label>
                <Form.Control type="text" value={ map } onChange={ (e) => setMap(e.target.value) }/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Image</Form.Label>
                <Form.Control type="file" className="form-control" ref={ fileRef } accept="image/*"/>
            </Form.Group>
            <Button type="submit" variant="success">Add</Button>&nbsp;
            <Button type="submit" variant="warning" href='/'>Cancel</Button>
    </Form>
    )
}

export default RestaurantCreatePage
