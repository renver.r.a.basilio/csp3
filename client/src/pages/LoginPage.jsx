import React, { useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import UserContext from '../contexts/UserContext'

import Swal from 'sweetalert2'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import Link from '@material-ui/core/Link'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

import Mutation from '../graphql/mutations'
import { GQLClient } from '../helpers'

const useStyles = makeStyles(theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/1600x900/?food)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

const LoginPage = () => {
    const classes = useStyles();

    const [isRedirected, setIsRedirected] = useState(false)
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { setUser } = useContext(UserContext)
    
    if (isRedirected) {
        return <Redirect to="/"/>
    }

    const login = (e) => {
        e.preventDefault()

        let variables = { email: username, password }

        GQLClient({}).request(Mutation.login, variables).then(data => {
            console.log(data)
            const result = data.login

            if (result !== null) {
                localStorage.setItem('id', result.id)
                localStorage.setItem('firstName', result.firstName)
                localStorage.setItem('lastName', result.lastName)
                localStorage.setItem('role', result.role)
                localStorage.setItem('token', result.token)

                setUser({
                    id: localStorage.getItem('id'),
                    firstName: localStorage.getItem('firstName'),
                    lastName: localStorage.getItem('lastName'),
                    role: localStorage.getItem('role'),
                    token: localStorage.getItem('token')
                })

                setIsRedirected(true)
            } else {
                Swal.fire('Login Failed', 'Either your email or password is incorrect, please try again', 'error')
            }
        })
    }

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={ (e) => login(e) }>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            value={ username }
                            onChange={ (e) => setUsername(e.target.value) }
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            value={ password }
                            onChange={ (e) => setPassword(e.target.value) }
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                        Sign In
                        </Button>
                        <Grid container>
                            <Grid item xs>
                            </Grid>
                            <Grid item>
                                <Link href="/register" variant="body2">
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    )
}

const Copyright = () => {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="/">
            Boom Parang Neneng B
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
    </Typography>
  )
}

export default LoginPage