import React, { useState, useContext} from 'react'
import UserContext from '../contexts/UserContext'

import Container from 'react-bootstrap/Container'
import Table from 'react-bootstrap/Table'

import Query from '../graphql/queries'
import { GQLClient } from '../helpers'
import { useEffect } from 'react'

const BookingUserHistoryPage = (props) => {
    const [booking, setBooking] = useState([])
    const { user } = useContext(UserContext)
    const options = {
        headers: {
            Authorization: 'Bearer ' + user.token
        }
    }

    useEffect(() => {
        GQLClient(options).request(Query.getBooking).then((data) => {
            let bookingRows = data.getBooking.map((booking) => {
                return (
                    <tr>
                        <td>{ booking.restaurantName }</td>
                        <td>{ booking.id }</td>
                        <td>{ booking.category }</td>
                        <td>{ booking.restaurantName }</td>
                        <td>{ booking.restaurantUnitPrice }</td>
                        <td>{ booking.paymentMode }</td>
                        <td>{ new Date(parseInt(booking.date)).toLocaleDateString() } </td>
                    </tr>
                )
            })

            setBooking(bookingRows)
        })
    }, [])
    return (
        <Container fluid>
            <h3>Bookings</h3>
            <Table bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Booking ID</th>
                        <th>Meal Time</th>
                        <th>Restaurant Name</th>
                        <th>Price</th>
                        <th>Payment Mode</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    { booking }
                </tbody>
            </Table>
        </Container>
    )
}

export default BookingUserHistoryPage