import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Swal from 'sweetalert2'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';

import InfoIcon from '@material-ui/icons/Info'
import RoomIcon from '@material-ui/icons/Room'

import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'
import { GQLClient, NodeServerURL } from '../helpers'

const RestaurantUpdatePage = (props) => {
    const restaurantId = props.match.params.id

    const [isRedirected, setIsRedirected] = useState(false)

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [place, setPlace] = useState('')
    const [unitPrice, setUnitPrice] = useState(0)
    const [cuisine, setCuisine] = useState('')
    const [map, setMap] = useState('')
    const [imageLocation, setImageLocation] = useState('')

    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
    setValue(newValue)
    }
    
    useEffect(() => {
        GQLClient({}).request(Query.getRestaurant, { id: restaurantId }).then((data) => {
            setName(data.getRestaurant.name)
            setDescription(data.getRestaurant.description)
            setPlace(data.getRestaurant.place)
            setUnitPrice(data.getRestaurant.unitPrice)
            setCuisine(data.getRestaurant.cuisine)
            setMap(data.getRestaurant.map)
            setImageLocation(data.getRestaurant.imageLocation)
        })
    }, [])

    const editRestaurantPage = (e) => {
    	e.preventDefault()

    	let variables = {
    		id: restaurantId,
    		name,
    		description,
    		place,
    		unitPrice: parseFloat(unitPrice),
    		cuisine,
    		map
    	}

    	GQLClient({}).request(Mutation.updateRestaurant, variables).then((data) => {
    		if (data.updateRestaurant) {
    			Swal.fire({
                        title: 'Restaurant Edited',
                        text: 'You will be redirected to the menu after closing this message',
                        icon: 'success'
                    }).then(() => {
                        setIsRedirected(true)
                    })    
            } else {
                Swal.fire({
                    title: 'Restaurant Update Failed',
                    text: 'The server encountered an error.',
                    icon: 'error'
                })
            }
    	}
    )}

    function TabPanel(props) {
        const { children, value, index, ...other } = props;
    
        return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-prevent-tabpanel-${index}`}
            aria-labelledby={`scrollable-prevent-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
        );
    }
    
    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    }
    
    function a11yProps(index) {
        return {
            id: `scrollable-prevent-tab-${index}`,
            'aria-controls': `scrollable-prevent-tabpanel-${index}`,
        }
    }

    if (isRedirected) {
        return <Redirect to='/'/>
    }

    return (
        <Container>
        	<Form onSubmit={ (e) => editRestaurantPage(e) }>
	            <h4> <img src={ NodeServerURL + imageLocation} style={{width: "30px"}} /> { name } @ { place }</h4>
	            <Form.Group>
		            <Form.Label>Restaurant Name</Form.Label>
		            <Form.Control type="text" value={ name } onChange={ (e) => setName(e.target.value) }></Form.Control>
	            </Form.Group>
	            <Form.Group>
		            <Form.Label>Restaurant Location</Form.Label>
		            <Form.Control type="text" value={ place } onChange={ (e) => setPlace(e.target.value) }></Form.Control>
	            </Form.Group>
	            <Row>
	                <Col sm={8}>
        	            

	                    <div style={{ float: "left" }}>
	                        <p>category<span className="ml-3 pr-3"> &#124;</span></p>
	                    </div>
	                    <div style={{ float: "left" }}>
	                        <p>&#8369;{ unitPrice }<span className="ml-3 pr-3"> &#124;</span></p>
	                    </div>
	                    <div>
	                        <p>{ cuisine }</p>
	                    </div>
	                    <div style={{lineHeight: "0.5", float: "left"}}>
	                        <p>Reservations: 50 <span className="ml-3 pr-3"> &#124;</span></p>
	                    </div>
	                    <div style={{lineHeight: "0.5"}}>
	                        <p>{ place }</p>
	                    </div>
	                    <img src={ NodeServerURL + imageLocation} style={{width: "730px"}} />
	                    <div>
	                        <AppBar position="static">
	                            <Tabs
	                            value={value}
	                            onChange={handleChange}
	                            variant="fullWidth"
	                            scrollButtons="off"
	                            >
	                                <Tab icon={<InfoIcon />}{...a11yProps(1)} />
	                                <Tab icon={<RoomIcon />}{...a11yProps(0)} /> 
	                            </Tabs>
	                        </AppBar>
	                        <TabPanel value={value} index={0}>
	                            <h5><strong>About</strong></h5>
	                            <p>{ description } Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt quas illum, quaerat ipsum, fuga esse nisi recusandae veritatis laborum nulla totam natus quibusdam sit ea ut doloribus excepturi. Similique, eaque?</p>
	                            <h5><strong>Operating Hours</strong></h5>
	                            <ul>
	                                <li>Lunch: 11:00 AM - 2:00 PM</li>
	                                <li>Dinner: 5:00 PM - 10:00 PM</li>
	                            </ul>
	                            <h5><strong>Special Condition</strong></h5>
	                            <ul>
	                                <li>All prices in PHP and are inclusive of VAT.</li>
	                                <li>Please present your Restoeat reservation notification upon arriving at the restaurant.</li>
	                                <li>Your reservation is only guaranteed within 15 minutes of your reservation time. Please make sure your party is complete when you arrive.</li>
	                                <li>Only the number of seats reserved in Bookito will be eligible for the discount.</li>
	                                <li>Discounts only apply to regular buffet rate. Drinks and discounted rates are NOT included in the discount. </li>
	                                <li>Restoeat discounts cannot be availed in conjunction with other discounts (Senior Citizen/PWD/in-house promo).</li>
	                                <li>Seating is subject to availability. You may be asked to wait if restaurant is full.</li>
	                            </ul>
	                            
	                        </TabPanel>
	                        <TabPanel value={value} index={1}>
	                            <p>Open Waze or Google Maps and enter our address to locate us. See you There!</p>
	                        </TabPanel>
	                    </div>
	                    <iframe src={ map } style={{width:"730px", height:"481px", frameborder:"0", border: "0"}}></iframe>
	                    <Button variant="primary" type="submit">Edit</Button>
	                    <Button variant="warning" type="submit" href="/">cancel</Button>
	                </Col>
	                <Col sm={4}>             
	                </Col>
	            </Row>
            </Form>
        </Container>
    )
}

export default RestaurantUpdatePage