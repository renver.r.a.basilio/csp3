import React, { useState, useEffect, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import UserContext from '../contexts/UserContext'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

import Swal from 'sweetalert2'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import 'date-fns';

import InfoIcon from '@material-ui/icons/Info'
import RoomIcon from '@material-ui/icons/Room'

import Query from '../graphql/queries'
import Mutation from '../graphql/mutations'
import { GQLClient, NodeServerURL } from '../helpers'

const BookingPage = (props) => {
    const restaurantId = props.match.params.id
    const { user } = useContext(UserContext)
    const options = {
        headers: {
            Authorization: 'Bearer ' + user.token
        }
    }

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [place, setPlace] = useState('')
    const [unitPrice, setUnitPrice] = useState(0)
    const [category, setCategory] = useState(null)
    const [categories, setCategories] = useState([])
    const [cuisine, setCuisine] = useState('')
    const [cuisines, setCuisines] = useState([])
    const [map, setMap] = useState('')
    const [imageLocation, setImageLocation] = useState('')
    const [date, setDate] = useState(null)
    const [isRedirected, setIsRedirected] = useState(false)

    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }
    
    useEffect(() => {
        GQLClient({}).request(Query.getRestaurant, { id: restaurantId }).then((data) => {
            setName(data.getRestaurant.name)
            setDescription(data.getRestaurant.description)
            setPlace(data.getRestaurant.place)
            setUnitPrice(data.getRestaurant.unitPrice)
            setCuisine(data.getRestaurant.cuisine)
            setMap(data.getRestaurant.map)
            setImageLocation(data.getRestaurant.imageLocation)
        })
        GQLClient({}).request(Query.getCategories, null).then((data) => {
            let options = data.getCategories.map((category) => {
                return <option key={ category.id } value={ category.name }>{ category.name }</option> 
            })
            setCategories(options)
        })
        GQLClient({}).request(Query.getCuisines, null).then((data) => {
            let options = data.getCuisines.map((cuisine) => {
                return <option key={ cuisine.id } value={ cuisine.name }>{ cuisine.name }</option> 
            })
            setCuisines(options)
        })
    }, [])


    if (isRedirected) {
        return <Redirect to={ '/' } />
    }

    const booking = (paymentMode) => {

        let variables = { 
            userId: user.id,
            userFirstName: user.firstName,
            userLastName: user.lastName,
            restaurantId: restaurantId,
            restaurantName: name,
            restaurantPlace: place,
            restaurantUnitPrice: parseFloat(unitPrice),
            date,
            category,
            paymentMode: paymentMode
        }

        GQLClient(options).request(Mutation.addBooking, variables)
        .then(data => {
            const bookingAdded = data.addBooking

            if (bookingAdded) {
                Swal.fire('Success', 'You will now be redirected to the menu page.','success').then(() => {
                    setIsRedirected(true)
                })
            } else {
                Swal.fire('Failed', 'Payment Failed', 'error')
            }
        })
    }

    function TabPanel(props) {
        const { children, value, index, ...other } = props;
    
        return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-prevent-tabpanel-${index}`}
            aria-labelledby={`scrollable-prevent-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
        );
    }
    
    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    }
    
    function a11yProps(index) {
        return {
            id: `scrollable-prevent-tab-${index}`,
            'aria-controls': `scrollable-prevent-tabpanel-${index}`,
        }
    }

    return (
        <Container>
            <h4> <img src={ NodeServerURL + imageLocation} style={{width: "30px"}} /> { name } @ { place }</h4>
            <Row>
                <Col sm={8}>
                    <div style={{ float: "left" }}>
                        <p> category <span className="ml-3 pr-3"> &#124;</span></p>
                    </div>
                    <div style={{ float: "left" }}>
                        <p>&#8369;{ unitPrice }<span className="ml-3 pr-3"> &#124;</span></p>
                    </div>
                    <div>
                        <p>{ cuisine }</p>
                    </div>
                    <div style={{lineHeight: "0.5", float: "left"}}>
                        <p>Reservations: 50 <span className="ml-3 pr-3"> &#124;</span></p>
                    </div>
                    <div style={{lineHeight: "0.5"}}>
                        <p>{ place }</p>
                    </div>
                    <img src={ NodeServerURL + imageLocation} style={{width: "730px"}} />
                    <div>
                        <AppBar position="static">
                            <Tabs
                            value={value}
                            onChange={handleChange}
                            variant="fullWidth"
                            scrollButtons="off"
                            >
                                <Tab icon={<InfoIcon />}{...a11yProps(1)} />
                                <Tab icon={<RoomIcon />}{...a11yProps(0)} /> 
                            </Tabs>
                        </AppBar>
                        <TabPanel value={value} index={0}>
                            <h5><strong>About</strong></h5>
                            <p>{ description } Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt quas illum, quaerat ipsum, fuga esse nisi recusandae veritatis laborum nulla totam natus quibusdam sit ea ut doloribus excepturi. Similique, eaque?</p>
                            <h5><strong>Operating Hours</strong></h5>
                            <p> Lorem ipsum dolor sit, amet consectetur adipisicing elit. Perferendis neque voluptatibus aliquid velit eligendi non maiores rerum necessitatibus quia modi eveniet, sapiente odit, assumenda eum quod quos beatae corporis minus.</p>
                            <h5><strong>Special Condition</strong></h5>
                            <ul>
                                <li>All prices in PHP and are inclusive of VAT.</li>
                                <li>Please present your Restoeat reservation notification upon arriving at the restaurant.</li>
                                <li>Your reservation is only guaranteed within 15 minutes of your reservation time. Please make sure your party is complete when you arrive.</li>
                                <li>Only the number of seats reserved in Bookito will be eligible for the discount.</li>
                                <li>Discounts only apply to regular buffet rate. Drinks and discounted rates are NOT included in the discount. </li>
                                <li>Restoeat discounts cannot be availed in conjunction with other discounts (Senior Citizen/PWD/in-house promo).</li>
                                <li>Seating is subject to availability. You may be asked to wait if restaurant is full.</li>
                            </ul>
                            
                        </TabPanel>
                        <TabPanel value={value} index={1}>
                            item
                        </TabPanel>
                    </div>
                    <iframe src={ map } style={{width:"730px", height:"481px", frameborder:"0", border: "0"}}></iframe>
                </Col>
                <Col sm={4}>
                    <h3 className="text-center">Reservation Details</h3>
                    <Form className="pt-2">
                        <Card>
                            <Card.Header className="text-center">Choose date and time</Card.Header>
                            <Form.Group>
                                <Form.Control type="date" value={ date } onChange={ (e) => setDate(e.target.value) } required/>
                            </Form.Group>
                            <Form.Group className="mx-5 pt-1">
                                <Form.Control as="select" onChange={ (e) => setCategory(e.target.value) } required>
                                <option value selected disabled>Select Meal Time</option>
                                    { categories }
                                </Form.Control>
                            </Form.Group>
                            <Button variant="primary" onClick={ (e) => booking('Cash') } type="button">Checkout Cash</Button>
                            <Button variant="primary" onClick={ (e) => booking('Stripe') } type="button">Checkout Stripe</Button>
                        </Card>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default BookingPage