import React, { useState, useContext } from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import { UserProvider } from './contexts/UserContext'

import AppNavbar from './components/AppNavbar'
import NotFoundPage from './pages/NotFoundPage'
import RegisterPage from './pages/RegisterPage'
import LoginPage from './pages/LoginPage'
import MenuPage from './pages/MenuPage'
import RestaurantCreatePage from './pages/RestaurantCreatePage'
import BookingPage from './pages/BookingPage'
import RestaurantUpdatePage from './pages/RestaurantUpdatePage'
import RestaurantDeletePage from './pages/RestaurantDeletePage'
import BookingUserHistoryPage from './pages/BookingUserHistoryPage'
import BookingsHistoryPage from './pages/BookingsHistoryPage'

const App = () => {
	const [user, setUser] = useState({
		id: localStorage.getItem('id'),
		firstName: localStorage.getItem('firstName'),
		lastName: localStorage.getItem('lastName'),
		role: localStorage.getItem('role'),
		token: localStorage.getItem('token')
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({ firstName: null, lastName: null, role: null, token: null })
	}

	const Load = (props, page) => {
		if (user.token === null) return <Redirect to='/login'/>

		if (page === 'LogoutPage') {
			unsetUser()
			return <Redirect to='/login'/>
		}

		switch (page) {
            case 'MenuPage': return <MenuPage { ...props }/>
            case 'RestaurantCreatePage': return <RestaurantCreatePage { ...props }/>
			case 'BookingPage': return <BookingPage { ...props }/>
			case 'BookingUserHistoryPage': return <BookingUserHistoryPage { ...props }/>
			case 'RestaurantUpdatePage': return <RestaurantUpdatePage { ...props }/>
			case 'RestaurantDeletePage': return <RestaurantDeletePage { ...props }/>
        }

	}

    return (
    	<UserProvider value={ { user, setUser, unsetUser } }>
	        <BrowserRouter>
	            <AppNavbar />
	            <Switch>
	                <Route exact path='/register' component={ RegisterPage }/>
	                <Route exact path='/login' component={ LoginPage }/>
	                <Route exact path='/' render={ (props) => Load(props, 'MenuPage') }/>
	                <Route exact path='/logout' render={ (props) => Load(props, 'LogoutPage') }/>
	                <Route exact path='/restaurant-create' render={ (props) => Load(props, 'RestaurantCreatePage') }/>
	                <Route exact path='/booking/:id' render={ (props) => Load(props, 'BookingPage') }/>
	                <Route exact path='/history/:id' render={ (props) => Load(props, 'BookingUserHistoryPage') }/>
	                <Route exact path='/restaurant-update/:id' render={ (props) => Load(props, 'RestaurantUpdatePage') }/>
					<Route exact path='/restaurant-delete/:id' render={ (props) => Load(props, 'RestaurantDeletePage') }/>
					<Route exact path='/bookings' component={ BookingsHistoryPage }/>
	                <Route component={ NotFoundPage }/>
	            </Switch>
	        </BrowserRouter>
	    </UserProvider>
    )
}

export default App;