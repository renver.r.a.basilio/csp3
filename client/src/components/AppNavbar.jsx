import React, { useContext } from 'react'
import UserContext from '../contexts/UserContext'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

const AppNavbar = (props) => {
	const { user } = useContext(UserContext)
	let navigation 

	if (user.token === null) {
		navigation = (
			<Nav className="ml-auto">
				<Nav.Link href="/register">Register</Nav.Link>
				<Nav.Link href="/login">Login</Nav.Link>
			</Nav>
		)
	} else {
		if (user.role === 'admin') {
			navigation = (
				<React.Fragment>
					<Nav className="mr-auto">
						<Nav.Link href="/">Menu</Nav.Link>
						<Nav.Link href="/bookings">Bookings</Nav.Link>
					</Nav>
					<Nav className="ml-auto">
						<Navbar.Brand>{ user.firstName }</Navbar.Brand>
						<Nav.Link href="/logout">Logout</Nav.Link>
					</Nav>
				</React.Fragment>
			)
		} else if (user.role === 'customer') {
			navigation = (
				<React.Fragment>
					<Nav className="mr-auto">
						<Nav.Link href="/">Menu</Nav.Link>
						<Nav.Link href={ "/history/" + user.id }>History</Nav.Link>
					</Nav>
					<Nav className="ml-auto">
						<Navbar.Brand>{ user.firstName }</Navbar.Brand>
						<Nav.Link href="/logout">Logout</Nav.Link>
					</Nav>
				</React.Fragment>
			)
		}
	}
    return (
    	<Navbar bg="dark" variant="dark">
    		<Navbar.Brand>Boom Parang Neneng B</Navbar.Brand>
    		<Navbar.Collapse>
    			{ navigation }
    		</Navbar.Collapse>
    	</Navbar>
    )
}

export default AppNavbar